//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname ,'index.html'))
});

app.post('/', function(req,res) {
  res.send("Hemos recibido su petición post")
});

app.put('/', function(req,res) {
  res.send("Hemos recibido su petición put cambiada")
});

app.delete('/', function(req,res) {
  res.send("Hemos recibido su petición delete")
});

app.get('/clientes/:idecliente',function (req,res) {
  res.send("Aqui tiene al cliente número "+req.params.idecliente);
});

//var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req,res){
  res.sendFile(path.join(__dirname ,'Movimientos.json'));
  //tambien sirve res.send(movimientosJSON);
});
